import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'w-value',
    templateUrl: './value.component.html',
    styleUrls: ['./value.component.scss']
})
export class ValueComponent implements OnInit {

    @Output()
    valueEmitter: EventEmitter<string> = new EventEmitter<string>();

    unit: string;

    value: number;

    constructor() {
        this.unit = 'px';
        this.value = 0;
    }

    ngOnInit() {
    }

    sendValue() {
        this.valueEmitter.emit(this.value + this.unit);
    }
}