import {Component, Input, OnInit} from '@angular/core';
import {log} from 'util';
import {st} from '@angular/core/src/render3';

@Component({
    selector: 'w-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

    layout;

    classNames: string[] = ['mix-input'];

    classNameBindings: string[] = ['iconClass'];

    placeholder: string = 'Placeholder';

    value: null;

    iconName: null;

    iconPlace: null;

    name: string = null;

    type: string = 'text';

    observeValue: null;

    changeValue: null;

    constructor() {
    }

    ngOnInit() {
    }

    change(value) {
        console.log(value);
    }
}
