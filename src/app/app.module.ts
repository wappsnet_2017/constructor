import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ValueComponent} from './components/value/value.component';
import {FormsModule} from '@angular/forms';
import { InputComponent } from './components/input/input.component';
import { RangeComponent } from './components/range/range.component';

@NgModule({
    declarations: [
        AppComponent,
        ValueComponent,
        InputComponent,
        RangeComponent,
    ],
    imports: [
        FormsModule,
        BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
