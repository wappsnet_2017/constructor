import {Component, ViewChild} from '@angular/core';
import {log} from 'util';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    properties: string[] = [];

    changeSize(value, target) {
        target.setAttribute('style', 'border-radius:' + value);
    }

    show(event) {
        let element = event.target;
        this.properties = [];
        console.log('length', element.style.length);
        for (let type in element.style) {
            this.properties.push('' + type);
        }
    }

    changeBorderRadius($event, target) {
        console.log($event);
        console.log(target);
        target.setAttribute('style', 'border-radius:' + $event.target.value + 'px');
        // target.style.borderTopLeftRadius = $event.value + 'px';
        // console.log(target.style.borderTopLeftRadius);
    }
}
